
Pod::Spec.new do |s|
  s.name         = "ISUtilsPack"
  s.version      = "2.0.1"
  s.summary      = "ISSupport is a pod for communication inside up with support team."

  s.description  = <<-DESC
                   A longer description of ISSupport in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "http://bitbucket.org/sjkostyan/utilspack"
  s.license      = "MIT"
  s.author       = { "Сергей Костян" => "sjsoad666@yandex.ru" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/sjkostyan/utilspack", :tag => s.version.to_s }
  s.source_files = "UtilsPack/UtilsPack/utils/*.{h,m}"
  s.requires_arc = true
  s.dependency "SVProgressHUD"
  s.dependency "AFNetworking"

end
