//
//  main.m
//  UtilsPack
//
//  Created by Сергей Костян on 02.06.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ISAppDelegate class]));
    }
}
