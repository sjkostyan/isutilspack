//
//  UIImage+Color.h
//
//  Created by Sam McEwan sammcewan@me.com on 15/10/12.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

- (UIImage*)changeColor:(UIColor*)color;
+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage;

@end
