//
//  NSDictionary+NSNull.h
//  YesOrNext
//
//  Created by Валентин Тоцкий on 10.01.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSNull)

- (id)objectForKeyNotNull:(id)key;

@end
