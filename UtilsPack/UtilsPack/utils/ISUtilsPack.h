//
//  ISUtilsPack.h
//  UtilsPack
//
//  Created by Сергей Костян on 02.06.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#ifndef UtilsPack_ISUtilsPack_h
#define UtilsPack_ISUtilsPack_h

#import "NSArray+NSNull.h"
#import "NSDictionary+NSNull.h"
#import "UIImage+Color.h"
#import "UIView+position.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>

#import "ISBaseApplicationManager.h"

#pragma mark - defines -

//folder
#define kDocumentsDirectory [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define kCachesDirectory    [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]

//device type definition
#define IS_IPAD             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

//system version
#define SYS_VERSION         [[UIDevice currentDevice].systemVersion floatValue]
#define IS_IOS7             (SYS_VERSION >= 7.0 && SYS_VERSION<8.0)
#define IS_IOS8             (SYS_VERSION >=8.0)

//screen resolutions
#define SCREEN_WIDTH        ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT       ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH   (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH   (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

//+device
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5         (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6         (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P        (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

//orientation
#define IS_PORTRAIT         (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
#define IS_LANDSCAPE        (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))

//shortands
#define IS_CLASS(x, y)      [x isKindOfClass:[y class]]
#define COLOR_WITH_WHITE(v) [UIColor colorWithWhite:(float)(v)/255.0f alpha:1.0f]
#define COLOR(r,g,b)        [UIColor colorWithRed:(float)(r)/255.0f green:(float)(g)/255.0f blue:(float)(b)/255.0f alpha:1.0f]
#define LS(string)          NSLocalizedString(string, nil)

#endif
