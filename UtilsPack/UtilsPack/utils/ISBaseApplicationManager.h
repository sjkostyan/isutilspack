//
//  ISBaseApplicationManager.h
//  UtilsPack
//
//  Created by Сергей Костян on 13.02.15.
//  Copyright (c) 2015 Inteza. All rights reserved.
//

#import "ISUtilsPack.h"

@interface ISBaseApplicationManager : NSObject

+ (instancetype) sharedManager;
+ (void)registerAppForPushNotifications;
+ (BOOL)isDebug;

- (CGSize) screenSize;

@end
