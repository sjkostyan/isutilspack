//
//  NSArray+NSNull.m
//  YesOrNext
//
//  Created by Валентин Тоцкий on 10.01.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "NSArray+NSNull.h"

@implementation NSArray (NSNull)

- (id)objectAtIndexNotNull:(NSInteger) index {
    id object = [self objectAtIndex:index];
    if ([object isKindOfClass:[NSNull class]])
        return nil;
    
    return object;
}

@end
