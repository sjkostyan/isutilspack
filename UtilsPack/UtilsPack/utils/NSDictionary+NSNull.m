//
//  NSDictionary+NSNull.m
//  YesOrNext
//
//  Created by Валентин Тоцкий on 10.01.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import "NSDictionary+NSNull.h"

@implementation NSDictionary (NSNull)

// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key {
    id object = [self objectForKey:key];
    if ([object isKindOfClass:[NSNull class]])
        return nil;
    
    return object;
}


@end
