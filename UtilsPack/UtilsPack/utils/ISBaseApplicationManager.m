//
//  ISBaseApplicationManager.m
//  UtilsPack
//
//  Created by Сергей Костян on 13.02.15.
//  Copyright (c) 2015 Inteza. All rights reserved.
//

#import "ISBaseApplicationManager.h"

@implementation ISBaseApplicationManager

+ (instancetype) sharedManager {
    static ISBaseApplicationManager* manager = nil;
    if(!manager){
        manager = [[ISBaseApplicationManager alloc] init];
    }
    return manager;
}

+ (void)registerAppForPushNotifications
{
    if(![[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)])
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    else
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

+ (BOOL)isDebug
{
#ifdef DEBUG
    return YES;
#else
    return NO;
#endif
}

- (CGSize) screenSize
{
    // return actual screen size for iOS 8
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if (IS_IOS7)
    {
        // define screen size for iOS 7
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        if (IS_PORTRAIT)
        {
            // in portrait width < height
            screenSize = CGSizeMake(MIN(screenWidth, screenHeight),
                                    MAX(screenWidth, screenHeight));
        }
        else
        {
            // in landscape width > height
            screenSize = CGSizeMake(MAX(screenWidth, screenHeight),
                                    MIN(screenWidth, screenHeight));
        }
    }
    
    return screenSize;
}

@end
