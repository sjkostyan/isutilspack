//
//  ISViewController.h
//  UtilsPack
//
//  Created by Сергей Костян on 02.06.14.
//  Copyright (c) 2014 Inteza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISViewController : UIViewController

@property (strong) IBOutlet UIVisualEffectView *blured;

@end
